<?php namespace Ae\Visitstory\Tests\Unit\Models;

use Ae\User\Models\User;
use Ae\User\Models\UserGroup;
use Ae\Visitstory\Helpers\GroupHelper;
use Ae\Visitstory\Helpers\UserHelper;
use Ae\Visitstory\Interfaces\ContestAvailableContestants;
use Ae\Visitstory\Interfaces\ContestTypes;
use Ae\Visitstory\Models\Contest;
use Ae\Visitstory\Models\ContestPopularityRule;
use Ae\Visitstory\Models\Post;
use Ae\Visitstory\Models\PostType;
use Illuminate\Database\Eloquent\Collection;
use October\Rain\Argon\Argon;

class ContestTest extends \PluginTestCase
{
    /**
     * @runTestsInSeparateProcess
     */
    public function testGetPopularityContests()
    {
        $pastDate = new Argon('now');
        $now = new Argon('now');

        $pastDate->subDay(Contest::POPULARITY_CONTEST_DURATION * 3);
        $contest1 = Contest::createNewPopularityContest($pastDate);
        $contest1->finish();

        $pastDate->subDay(Contest::POPULARITY_CONTEST_DURATION);
        $contest2 = Contest::createNewPopularityContest($pastDate);
        $contest2->finish();

        $pastDate->subDay(Contest::POPULARITY_CONTEST_DURATION);
        $contest3 = Contest::createNewPopularityContest($pastDate);
        $contest3->finish();

        $activeContest1 = Contest::createNewPopularityContest($now);

        $now->subDay(Contest::POPULARITY_CONTEST_DURATION);
        $activeContest2 = Contest::createNewPopularityContest($now);

        $start = new Argon('now');
        $start->subDay();
        $end = new Argon('now');
        $end->addDay();

        $regularContest = new Contest([
            'name' => 'test 2',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start,
            'date_to' => $end
        ]);
        $regularContest->save();

        $activePopularityContests = Contest::getPopularityContests();
        $this->assertTrue($activePopularityContests->contains($activeContest1->id));
        $this->assertTrue($activePopularityContests->contains($activeContest2->id));

        $this->assertFalse($activePopularityContests->contains($contest1->id));
        $this->assertFalse($activePopularityContests->contains($contest2->id));
        $this->assertFalse($activePopularityContests->contains($contest3->id));

        $this->assertFalse($activePopularityContests->contains($regularContest->id));

        $archivedPopularityContests = Contest::getPopularityContests(Contest::DISPLAY_ARCHIVE);
        $this->assertFalse($archivedPopularityContests->contains($activeContest1->id));
        $this->assertFalse($archivedPopularityContests->contains($activeContest2->id));

        $this->assertTrue($archivedPopularityContests->contains($contest1->id));
        $this->assertTrue($archivedPopularityContests->contains($contest2->id));
        $this->assertTrue($archivedPopularityContests->contains($contest3->id));

        $this->assertFalse($archivedPopularityContests->contains($regularContest->id));
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testFinishingContest()
    {
        $lastCycle = new Argon('now');
        $lastCycle->subDay(Contest::POPULARITY_CONTEST_DURATION);

        $contest = Contest::createNewPopularityContest($lastCycle);
        $this->assertTrue($contest->isActive());

        $contest->finish();
        $this->assertFalse($contest->isActive());
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testGetLastPopularityContest()
    {
        $pastDate = new Argon('now');
        $pastDate->subDay();

        $contest1 = Contest::createNewPopularityContest($pastDate);

        $pastDate->subDays(Contest::POPULARITY_CONTEST_DURATION);
        $contest2 = Contest::createNewPopularityContest($pastDate);

        $pastDate->subDays(Contest::POPULARITY_CONTEST_DURATION);
        $contest3 = Contest::createNewPopularityContest($pastDate);
        $contest3->finish();

        $test1 = Contest::getLastPopularityContest();
        $this->assertEquals($contest2->id, $test1->id);
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testGetCurrentPopularityContestNullResult()
    {
        //there is no contest available
        $test = Contest::getCurrentPopularityContest();

        $this->assertNull($test);
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testGetLastPopularityContestNullResult()
    {
        //there is no contest available
        $test = Contest::getLastPopularityContest();

        $this->assertNull($test);
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testGetArchivedRegularContests()
    {
        $start1 = new Argon('now');
        $start1->subDays(100);
        $end1 = new Argon('now');
        $end1->subDays(90);

        $regularContest1 = new Contest([
            'name' => 'test 1',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start1,
            'date_to' => $end1
        ]);
        $regularContest1->save();

        $start2 = new Argon('now');
        $start2->subDays(100);
        $end2 = new Argon('now');
        $end2->subDays(80);

        $regularContest2 = new Contest([
            'name' => 'test 2',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start2,
            'date_to' => $end2
        ]);
        $regularContest2->save();

        $start3 = new Argon('now');
        $start3->subDays(100);
        $end3 = new Argon('now');
        $end3->subDays(80);

        $regularContest3 = new Contest([
            'name' => 'test 3',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start3,
            'date_to' => $end3
        ]);
        $regularContest3->save();

        $start4 = new Argon('now');
        $start4->subDays(100);
        $end4 = new Argon('now');
        $end4->subDays(80);

        $regularContest4 = new Contest([
            'name' => 'test 4',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start4,
            'date_to' => $end4
        ]);
        $regularContest4->save();

        $regularContest1->finish();
        $regularContest3->finish();

        $regularContest1->reload();
        $regularContest2->reload();
        $regularContest3->reload();
        $regularContest4->reload();

        $archivedRegularContests = Contest::getArchivedRegularContests();

        $this->assertTrue($archivedRegularContests->contains($regularContest1->id));
        $this->assertTrue($archivedRegularContests->contains($regularContest3->id));

        $this->assertFalse($archivedRegularContests->contains($regularContest2->id));
        $this->assertFalse($archivedRegularContests->contains($regularContest4->id));
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testCreateNewPopularityContestException()
    {
        Contest::createNewPopularityContest();
        $this->setExpectedException(\LogicException::class);
        Contest::createNewPopularityContest();
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testGetArchiveList()
    {
        $pastDate = new Argon('now');
        $now = new Argon('now');

        $pastDate->subDay(Contest::POPULARITY_CONTEST_DURATION * 3);
        $contest1 = Contest::createNewPopularityContest($pastDate);
        $contest1->finish();

        $pastDate->subDay(Contest::POPULARITY_CONTEST_DURATION);
        $contest2 = Contest::createNewPopularityContest($pastDate);
        $contest2->finish();

        $pastDate->subDay(Contest::POPULARITY_CONTEST_DURATION);
        $contest3 = Contest::createNewPopularityContest($pastDate);
        $contest3->finish();

        $activeContest1 = Contest::createNewPopularityContest($now);

        $now->subDay(Contest::POPULARITY_CONTEST_DURATION);
        $activeContest2 = Contest::createNewPopularityContest($now);

        $start = new Argon('now');
        $start->subDay();
        $end = new Argon('now');
        $end->addDay();

        $regularContest = new Contest([
            'name' => 'test 1',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start,
            'date_to' => $end
        ]);
        $regularContest->save();

        $start2 = new Argon('now');
        $start2->subDay();
        $end2 = new Argon('now');
        $end2->addDay();

        $regularContest2 = new Contest([
            'name' => 'test 2',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start2,
            'date_to' => $end2
        ]);
        $regularContest2->save();
        $regularContest2->finish();

        $archivedContests = Contest::getArchiveList();

        $this->assertTrue($archivedContests->contains($regularContest2->id));

        $this->assertFalse($archivedContests->contains($contest1->id));
        $this->assertFalse($archivedContests->contains($contest2->id));
        $this->assertFalse($archivedContests->contains($contest3->id));
        $this->assertFalse($archivedContests->contains($activeContest1->id));
        $this->assertFalse($archivedContests->contains($activeContest2->id));
        $this->assertFalse($archivedContests->contains($regularContest->id));
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testGetList()
    {
        $pastDate = new Argon('now');
        $now = new Argon('now');

        $pastDate->subDay(Contest::POPULARITY_CONTEST_DURATION * 3);
        $contest1 = Contest::createNewPopularityContest($pastDate);
        $contest1->finish();

        $pastDate->subDay(Contest::POPULARITY_CONTEST_DURATION);
        $contest2 = Contest::createNewPopularityContest($pastDate);
        $contest2->finish();

        $pastDate->subDay(Contest::POPULARITY_CONTEST_DURATION);
        $contest3 = Contest::createNewPopularityContest($pastDate);
        $contest3->finish();

        $activeContest1 = Contest::createNewPopularityContest($now);

        $now->subDay(Contest::POPULARITY_CONTEST_DURATION);
        $activeContest2 = Contest::createNewPopularityContest($now);

        $start = new Argon('now');
        $start->subDay();
        $end = new Argon('now');
        $end->addDay();

        $regularContest = new Contest([
            'name' => 'test 1',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start,
            'date_to' => $end
        ]);
        $regularContest->save();

        $start2 = new Argon('now');
        $start2->subDay();
        $end2 = new Argon('now');
        $end2->addDay();

        $regularContest2 = new Contest([
            'name' => 'test 2',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start2,
            'date_to' => $end2
        ]);
        $regularContest2->save();

        $start3 = new Argon('now');
        $start3->subDay();
        $end3 = new Argon('now');
        $end3->addDay();

        $regularContest3 = new Contest([
            'name' => 'test 3',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start3,
            'date_to' => $end3
        ]);
        $regularContest3->save();
        $regularContest3->finish();

        $archivedContests = Contest::getList();

        $this->assertTrue($archivedContests->contains($activeContest2->id));
        $this->assertTrue($archivedContests->contains($regularContest->id));
        $this->assertTrue($archivedContests->contains($regularContest2->id));

        $this->assertFalse($archivedContests->contains($contest1->id));
        $this->assertFalse($archivedContests->contains($contest2->id));
        $this->assertFalse($archivedContests->contains($contest3->id));
        $this->assertFalse($archivedContests->contains($activeContest1->id));
        $this->assertFalse($archivedContests->contains($regularContest3->id));
    }

    public function testGetAwards()
    {
        $start = new Argon('now');
        $start->subDay();
        $end = new Argon('now');
        $end->addDay();

        $regularContest = new Contest([
            'name' => 'test 1',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start,
            'date_to' => $end
        ]);
        $regularContest->awards = [
            'name' => 'test1',
            'description' => 'test2',
            'image' => ''
        ];
        $regularContest->save();

        $regularContest->reload();
        $this->assertEquals([
            'name' => 'test1',
            'description' => 'test2',
            'image' => ''
        ], $regularContest->getAwards());
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testCanSignUp()
    {
        $start1 = new Argon('now');
        $start1->subDay();
        $end1 = new Argon('now');
        $end1->addDay();

        $contestForVisit = new Contest([
            'name' => 'test 1',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start1,
            'date_to' => $end1,
            'available_for' => ContestAvailableContestants::TYPE_VISIT
        ]);
        $contestForVisit->save();

        $start2 = new Argon('now');
        $start2->subDay();
        $end2 = new Argon('now');
        $end2->addDay();

        $contestForStory = new Contest([
            'name' => 'test 2',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start2,
            'date_to' => $end2,
            'available_for' => ContestAvailableContestants::TYPE_STORY
        ]);
        $contestForStory->save();

        $start3 = new Argon('now');
        $start3->subDay();
        $end3 = new Argon('now');
        $end3->addDay();

        $contestForVisitAndStory = new Contest([
            'name' => 'test 3',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start3,
            'date_to' => $end3,
            'available_for' => ContestAvailableContestants::TYPE_VISIT_AND_STORY
        ]);
        $contestForVisitAndStory->save();

        $visitUser = $this->createNewUser();
        $visitUserPost1 = $this->createNewPost($visitUser);
        $visitUserPost2 = $this->createNewPost($visitUser);
        $storyUser = $this->createNewUser(GroupHelper::instance()->getStoryGroup());
        $storyUserPost1 = $this->createNewPost($storyUser);
        $authorUser = $this->createNewUser(GroupHelper::instance()->getAuthorGroup());
        $authorUserPost1 = $this->createNewPost($authorUser);
        $redactorUser = $this->createNewUser(GroupHelper::instance()->getRedactorGroup());
        $redactorUserPost1 = $this->createNewPost($redactorUser);

        $this->assertTrue($contestForVisit->canSignUp($visitUser, $visitUserPost1));
        $this->assertTrue($contestForVisit->canSignUp($visitUser, $visitUserPost2));
        $this->assertFalse($contestForVisit->canSignUp($storyUser, $storyUserPost1));
        $this->assertFalse($contestForVisit->canSignUp($authorUser, $authorUserPost1));
        $this->assertFalse($contestForVisit->canSignUp($redactorUser, $redactorUserPost1));

        $this->assertFalse($contestForStory->canSignUp($visitUser, $visitUserPost1));
        $this->assertFalse($contestForStory->canSignUp($visitUser, $visitUserPost2));
        $this->assertTrue($contestForStory->canSignUp($storyUser, $storyUserPost1));
        $this->assertTrue($contestForStory->canSignUp($authorUser, $authorUserPost1));
        $this->assertFalse($contestForStory->canSignUp($redactorUser, $redactorUserPost1));

        $this->assertTrue($contestForVisitAndStory->canSignUp($visitUser, $visitUserPost1));
        $this->assertTrue($contestForVisitAndStory->canSignUp($visitUser, $visitUserPost2));
        $this->assertTrue($contestForVisitAndStory->canSignUp($storyUser, $storyUserPost1));
        $this->assertTrue($contestForVisitAndStory->canSignUp($authorUser, $authorUserPost1));
        $this->assertFalse($contestForVisitAndStory->canSignUp($redactorUser, $redactorUserPost1));

        $contestForVisit->sign($visitUserPost1);
        $contestForVisit = Contest::find(1);
        $this->assertFalse($contestForVisit->canSignUp($visitUser, $visitUserPost1));
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testIsAvailableFor()
    {
        $start1 = new Argon('now');
        $start1->subDay();
        $end1 = new Argon('now');
        $end1->addDay();

        $contestForVisit = new Contest([
            'name' => 'test 1',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start1,
            'date_to' => $end1,
            'available_for' => ContestAvailableContestants::TYPE_VISIT_AND_STORY
        ]);
        $contestForVisit->save();

        $visitUser = $this->createNewUser();
        $storyUser = $this->createNewUser(GroupHelper::instance()->getStoryGroup());
        $authorUser = $this->createNewUser(GroupHelper::instance()->getAuthorGroup());

        $this->assertFalse($contestForVisit->isAvailableFor(null));
        $this->assertTrue($contestForVisit->isAvailableFor($visitUser));
        $this->assertTrue($contestForVisit->isAvailableFor($storyUser));
        $this->assertTrue($contestForVisit->isAvailableFor($authorUser));
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testGetContestantsList()
    {
        $pastDate = new Argon('now');
        $contest = Contest::createNewPopularityContest($pastDate);

        $visitUser1 = $this->createNewUser();
        $visitUser1Post1 = $this->createNewPost($visitUser1);
        $visitUser1Post2 = $this->createNewPost($visitUser1);

        $visitUser2 = $this->createNewUser();
        $visitUser2Post1 = $this->createNewPost($visitUser2);
        $visitUser2Post2 = $this->createNewPost($visitUser2);

        $visitUser3 = $this->createNewUser();
        $visitUser3Post1 = $this->createNewPost($visitUser3);

        $contest->sign($visitUser1Post1);
        $contest->sign($visitUser1Post2);
        $contest->sign($visitUser2Post1);
        $contest->sign($visitUser2Post2);
        $contest->sign($visitUser3Post1);

        //reload associations
        $contest = Contest::find(1);
        //set points of contests
        foreach ($contest->posts as $contest_post) {
            if ($contest_post->id == $visitUser1Post2->id) {
                $contest_post->addLikes(5);
                $contest_post->addShares(5);
                $contest_post->addComments(5);
                $contest_post->addViews(5);
                $contest_post->save();
            } elseif ($contest_post->id == $visitUser1Post1->id) {
                $contest_post->addLikes(3);
                $contest_post->addShares(3);
                $contest_post->addComments(3);
                $contest_post->addViews(3);
                $contest_post->save();
            } elseif ($contest_post->id == $visitUser2Post2->id) {
                $contest_post->addLikes(2);
                $contest_post->addShares(2);
                $contest_post->addComments(2);
                $contest_post->addViews(2);
                $contest_post->save();
            }
        }
        //reload associations
        $contest = Contest::find(1);

        $contestantsList = $contest->getContestantsList();

        $this->assertEquals($contestantsList->get(0)['user']->id, $visitUser1->id);
        $this->assertEquals($contestantsList->get(1)['user']->id, $visitUser2->id);
        $this->assertEquals($contestantsList->get(2)['user']->id, $visitUser3->id);
        $this->assertEquals($contestantsList->get(0)['views'], 8);
        $this->assertEquals($contestantsList->get(2)['views'], 0);
        $this->assertTrue($contestantsList->get(0)['contested_posts']->contains($visitUser1Post1));
        $this->assertTrue($contestantsList->get(0)['contested_posts']->contains($visitUser1Post2));
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testGetPositionList()
    {
        $pastDate = new Argon('now');
        $contest = Contest::createNewPopularityContest($pastDate);

        $visitUser1 = $this->createNewUser();
        $visitUser1Post1 = $this->createNewPost($visitUser1);
        $visitUser1Post2 = $this->createNewPost($visitUser1);

        $visitUser2 = $this->createNewUser();
        $visitUser2Post1 = $this->createNewPost($visitUser2);
        $visitUser2Post2 = $this->createNewPost($visitUser2);

        $visitUser3 = $this->createNewUser();
        $visitUser3Post1 = $this->createNewPost($visitUser3);

        $contest->sign($visitUser1Post1);
        $contest->sign($visitUser1Post2);
        $contest->sign($visitUser2Post1);
        $contest->sign($visitUser2Post2);
        $contest->sign($visitUser3Post1);

        //reload associations
        $contest = Contest::find(1);
        //set points of contests
        foreach ($contest->posts as $contest_post) {
            if ($contest_post->id == $visitUser1Post2->id) {
                $contest_post->addLikes(5);
                $contest_post->addShares(5);
                $contest_post->addComments(5);
                $contest_post->addViews(5);
                $contest_post->save();
            } elseif ($contest_post->id == $visitUser2Post1->id) {
                $contest_post->addLikes(3);
                $contest_post->addShares(3);
                $contest_post->addComments(3);
                $contest_post->addViews(3);
                $contest_post->save();
            } elseif ($contest_post->id == $visitUser3Post1->id) {
                $contest_post->addLikes(3);
                $contest_post->addShares(3);
                $contest_post->addComments(3);
                $contest_post->addViews(3);
                $contest_post->save();
            }
        }
        //reload associations
        $contest = Contest::find(1);
        $positionList = $contest->getPositionList();

        $this->assertTrue($this->has($visitUser1, $positionList[1]['contestants']));
        $this->assertTrue($this->has($visitUser2, $positionList[2]['contestants']));
        $this->assertTrue($this->has($visitUser3, $positionList[2]['contestants']));
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testGetWinners()
    {
        $pastDate = new Argon('now');
        $contest = Contest::createNewPopularityContest($pastDate);

        $visitUser1 = $this->createNewUser();
        $visitUser1Post1 = $this->createNewPost($visitUser1);
        $visitUser1Post2 = $this->createNewPost($visitUser1);

        $visitUser2 = $this->createNewUser();
        $visitUser2Post1 = $this->createNewPost($visitUser2);
        $visitUser2Post2 = $this->createNewPost($visitUser2);

        $visitUser3 = $this->createNewUser();
        $visitUser3Post1 = $this->createNewPost($visitUser3);

        $contest->sign($visitUser1Post1);
        $contest->sign($visitUser1Post2);
        $contest->sign($visitUser2Post1);
        $contest->sign($visitUser2Post2);
        $contest->sign($visitUser3Post1);

        //reload associations
        $contest = Contest::find(1);
        //set points of contests
        foreach ($contest->posts as $contest_post) {
            if ($contest_post->id == $visitUser1Post2->id) {
                $contest_post->addLikes(5);
                $contest_post->addShares(5);
                $contest_post->addComments(5);
                $contest_post->addViews(5);
                $contest_post->save();
            } elseif ($contest_post->id == $visitUser2Post1->id) {
                $contest_post->addLikes(3);
                $contest_post->addShares(3);
                $contest_post->addComments(3);
                $contest_post->addViews(3);
                $contest_post->save();
            } elseif ($contest_post->id == $visitUser3Post1->id) {
                $contest_post->addLikes(3);
                $contest_post->addShares(3);
                $contest_post->addComments(3);
                $contest_post->addViews(3);
                $contest_post->save();
            }
        }

        $contest = Contest::find(1);
        list($winners, $allContestants) = $contest->getWinners();
        $this->assertEquals(85, key($winners[1]));
        $this->assertFalse(array_key_exists(2, $winners));
        /** @var $allContestants Collection */
        $this->assertNotFalse($allContestants->search(function($item, $key) { return (bool) $item['user']->id == 1; }));
        $this->assertNotFalse($allContestants->search(function($item, $key) { return (bool) $item['user']->id == 2; }));

        $contest->no_of_winners = 2;
        $contest->save();
        $contest = Contest::find(1);
        list($winners, $allContestants) = $contest->getWinners();
        $this->assertEquals(85, key($winners[1]));
        $this->assertEquals(51, key($winners[2]));
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testGetApprovedPosts()
    {
        $start1 = new Argon('now');
        $start1->subDay();
        $end1 = new Argon('now');
        $end1->addDay();

        $contestForVisit = new Contest([
            'name' => 'test 1',
            'type' => ContestTypes::TYPE_REGULAR,
            'date_from' => $start1,
            'date_to' => $end1,
            'available_for' => ContestAvailableContestants::TYPE_VISIT_AND_STORY
        ]);
        $contestForVisit->save();

        $visitUser1 = $this->createNewUser();
        $visitUser1Post1 = $this->createNewPost($visitUser1);
        $visitUser1Post2 = $this->createNewPost($visitUser1);
        $visitUser1Post3 = $this->createNewPost($visitUser1);
        $visitUser1Post4 = $this->createNewPost($visitUser1);
        $visitUser1Post5 = $this->createNewPost($visitUser1);

        $contestForVisit->sign($visitUser1Post1);
        $contestForVisit->sign($visitUser1Post2);
        $contestForVisit->sign($visitUser1Post3);
        $contestForVisit->sign($visitUser1Post4);
        $contestForVisit->sign($visitUser1Post5);

        $visitUser2 = $this->createNewUser();
        $visitUser2Post1 = $this->createNewPost($visitUser2);
        $visitUser2Post2 = $this->createNewPost($visitUser2);
        $visitUser2Post3 = $this->createNewPost($visitUser2);
        $visitUser2Post4 = $this->createNewPost($visitUser2);
        $visitUser2Post5 = $this->createNewPost($visitUser2);

        $contestForVisit->sign($visitUser2Post1);
        $contestForVisit->sign($visitUser2Post2);
        $contestForVisit->sign($visitUser2Post3);
        $contestForVisit->sign($visitUser2Post4);
        $contestForVisit->sign($visitUser2Post5);

        $contest = Contest::find(1);
        $toActivate = [
            $visitUser1Post1->id,
            $visitUser1Post3->id,
            $visitUser1Post5->id,
            $visitUser2Post2->id,
            $visitUser2Post4->id
        ];

        foreach ($contest->posts as $contestPost) {
            if (in_array($contestPost->post_id, $toActivate)) {
                $contestPost->activate();
            }
        }

        $contest = Contest::find(1);
        $test = $contest->getApprovedPosts();

        $this->assertTrue($this->has($visitUser1Post1, $test));
        $this->assertFalse($this->has($visitUser1Post2, $test));
        $this->assertTrue($this->has($visitUser1Post3, $test));
        $this->assertFalse($this->has($visitUser1Post4, $test));
        $this->assertTrue($this->has($visitUser1Post5, $test));

        $this->assertFalse($this->has($visitUser2Post1, $test));
        $this->assertTrue($this->has($visitUser2Post2, $test));
        $this->assertFalse($this->has($visitUser2Post3, $test));
        $this->assertTrue($this->has($visitUser2Post4, $test));
        $this->assertFalse($this->has($visitUser2Post5, $test));
    }

    /**
     * @runTestsInSeparateProcess
     */
    public function testPrepareArchiveListPaginationData()
    {
        $start1 = new Argon('now');
        $start1->subWeek();
        $end1 = new Argon('now');
        $end1->subDay();

        for ($x = 1; $x <= 50; $x++) {
            $contest = new Contest([
                'name' => 'test ' . $x,
                'type' => ContestTypes::TYPE_REGULAR,
                'date_from' => $start1,
                'date_to' => $end1,
                'available_for' => ContestAvailableContestants::TYPE_VISIT_AND_STORY
            ]);
            $contest->save();
            $contest->finish();
        }

        $test = Contest::prepareArchiveListPaginationData();

        $this->assertTrue(is_array($test));
        $this->assertEquals(50, $test['total']);
        $this->assertEquals(10, $test['pages_available']);
    }

    /**
     * @param UserGroup|null $group
     * @return User
     */
    private function createNewUser(UserGroup $group = null)
    {
        $user = new User();
        $user->name = \Str::random(6);
        $user->surname = \Str::random(6);
        $user->email = \Str::random(6) . '@' . \Str::random(6) . '.com';
        $user->username = \Str::random(6);
        $pass = \Str::random(6);
        $user->password = $pass;
        $user->password_confirmation = $pass;
        $user->save();

        unset($user->password);

        //$user->reload();

        if ($group === null) {
            $group = GroupHelper::instance()->getVisitGroup();
        }

        $grps = $user->getGroups();

        foreach ($grps as $single) {
            $user->removeGroup($single);
        }

        $user->addGroup($group);
        $user->save();

        $user->reload();

        return $user;
    }

    /**
     * @param User $user
     * @param int $type
     * @return Post
     */
    private function createNewPost(User $user, $type = PostType::TYPE_TRIP_SHORT)
    {
        $post = new Post();
        $post->name = \Str::random(6);
        $post->slug = \Str::random(6);
        $post->user_id = $user->id;
        $post->type_id = $type;
        $post->category_id = 1;
        $post->save();

        $post->reload();

        return $post;
    }

    /**
     * @param $modelNeedle
     * @param array|Collection $modelHaystack
     * @return bool
     */
    private function has($modelNeedle, $modelHaystack)
    {
        foreach ($modelHaystack as $entry) {
            if ($entry->id == $modelNeedle->id) {
                return true;
            }
        }

        return false;
    }
}
