<?php

namespace App\Classes;

/**
 * Plik obsługujący import danych z hurtowni sprzedażowej (dane otrzymywane z ich ftp), wytworzony dla firmy Whitebox.
 * Data utworzenia: połowa 2017
 */

use Illuminate\Support\Facades\Log;

final class WholesaleJaguar extends AuthXmlApi
{
    protected $url;

    protected $token;

    protected $userDiscount;

    protected $markup = .1;

    public function __construct()
    {
        $this->url = env('WHOLESALE_JAGUAR_URL', '');
        $this->token = env('WHOLESALE_JAGUAR_TOKEN', '');
        $this->userDiscount = 1 - (float) app("SettingsHelper")->getSetting('WHOLESALE_JAGUAR_USER_DISCOUNT', 0);

        if ($this->userDiscount = 1) {
            $this->userDiscount = 0;
        }

        $this->markup = (float) app("SettingsHelper")->getSetting('WHOLESALE_JAGUAR_MARKUP', .1);

        if ($this->userDiscount > 1 || $this->userDiscount < 0) {
            throw new \Exception(__NAMESPACE__ . '/' . __CLASS__ . ': Wrong data passed as user discount!');
        }

        if (empty($this->url) || empty($this->token)) {
            throw new \Exception(__NAMESPACE__ . '/' . __CLASS__ . 'Not enough data passed in env file!');
        }

        parent::__construct();
    }

    /**
     * @param $simpleXmlElement
     */
    protected function process($simpleXmlElement)
    {
        try {
            if ($simpleXmlElement instanceof \SimpleXMLElement) {
                $referenceId = app("DictionaryHelper")->getId(
                    (string) $simpleXmlElement->vendo_code,
                    class_basename(static::class)
                );

                if (empty($referenceId)) {
                    //schedule product to add into prestashop and get it id
                    $scheduledProductId = $this->scheduleToAddToProducts([
                        'supplier_reference' => (string) $simpleXmlElement->vendo_code,
                        'name' => (string) $simpleXmlElement->name,
                        'price' => app("PriceHelper")->calculateExportPrice(
                            (float) $simpleXmlElement->price,
                            $this->userDiscount,
                            $this->markup
                        ),
                        'wholesale_price' => app("PriceHelper")->getDiscountedPrice(
                            (float) $simpleXmlElement->price,
                            $this->userDiscount
                        ),
                        'weight' => (float) $simpleXmlElement->weight,
                        'image' => (string) $simpleXmlElement->main_image
                        //'minimal_quantity' => (float) $simpleXmlElement->minimal_order,
                    ]);
                    $this->scheduledProducts++;

                    //schedule product combination to add
                    if (!empty($simpleXmlElement->available_colors) &&
                        !empty($simpleXmlElement->available_colors->{'list-item'}) &&
                        count($simpleXmlElement->available_colors->{'list-item'}) > 1
                    ) {
                        foreach ($simpleXmlElement->available_colors->{'list-item'} as $variation) {
                            $this->scheduleToAddToProductsCombinations([
                                'product_to_add_id' => (int) $scheduledProductId,
                                'product_to_add_name' => (string) $simpleXmlElement->name,
                                'product_to_add_reference' => (string) $simpleXmlElement->vendo_code,
                                'supplier_reference' => (string) $variation->vendo_code,
                                'name' => (string) $variation->color->name,
                                'price' => 0, //wplyw na cene
                                'wholesale_price' => app("PriceHelper")->getDiscountedPrice(
                                    (float) $simpleXmlElement->price,
                                    $this->userDiscount
                                ),
                                'type' => 'color',
                                'image' => (string) $variation->main_image
                            ]);
                            $this->scheduledCombinations++;
                            $this->scheduledUpdates++;
                        }
                    }
                } else {
                    foreach ($simpleXmlElement->available_colors->{'list-item'} as $variation) {
                        $referenceId = app("DictionaryHelper")->getId(
                            (string) $variation->vendo_code,
                            class_basename(static::class)
                        );
                        $productData = app("DictionaryHelper")->getCombinationReference($referenceId);
                        $newPrice = app("PriceHelper")->calculateExportPrice(
                            (float) $simpleXmlElement->price,
                            $this->userDiscount,
                            $this->markup
                        );

                        if ($productData['quantity'] != (int) $variation->availability_count || $productData['price'] != $newPrice) {
                            $this->scheduleUpdate([
                                'reference_id' => $referenceId,
                                'name' => (string) $variation->vendo_code,
                                'price' => app("PriceHelper")->calculateExportPrice(
                                    (float) $simpleXmlElement->price,
                                    $this->userDiscount,
                                    $this->markup
                                ),
                                'wholesale_price' => app("PriceHelper")->getDiscountedPrice(
                                    (float) $simpleXmlElement->price,
                                    $this->userDiscount
                                ),
                                'quantity' => (int) $variation->availability_count
                            ]);

                            $this->scheduledUpdates++;
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            Log::debug(__NAMESPACE__ . '/' . __CLASS__ . ': Exception during export: ' . $e->getMessage());
            //ignore this row, move to next one
        }
    }
}
