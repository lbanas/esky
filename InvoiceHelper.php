<?php namespace Ae\Medman\Helpers;

use \Ae\Medman\Models\SubscriptionUsage;
use \Ae\Medman\Classes\InvoiceGenerator;
use Log;
use October\Rain\Support\Traits\Singleton;

/**
 * Klasa pomocnicza do celów generowania faktury na podstawie subskrybcji
 * Data utworzenia: 1 kwartał 2017
 */

class InvoiceHelper extends Base
{
    use Singleton;

    /**
     * @param SubscriptionUsage $subscriptionUsage
     * @return boolean
     */
    public function generateSingle(SubscriptionUsage $subscriptionUsage)
    {
        try {
            $generator = new InvoiceGenerator();
            $generator->setAutoRenew(false);
            $generator->fetchOne($subscriptionUsage);
            $generator->process();

            return true;
        } catch (\Exception $e) {
            Log::critical('Unable to generate invoice! '
                . 'Exception: ' . $e->getMessage()
                . '. Trace: ' . $e->getTraceAsString());
        }

        return false;
    }
}
