<?php

namespace Worker;

/**
 * Plik obsługuje aplikację przeglądającą logi i wychwytującą odpowiednie wpisy, utworzony dla firmy Rafcom do celów raportowych.
 * Data utworzenia: 1 kwartał 2016
 */

use Doctrine\DBAL\Connection;
use Silex\Application;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProcessController
 * @package Worker
 */
class ProcessController
{
    /**
     * @var bool
     */
    private static $debug = true;

    /**
     * @var bool|array
     */
    private $config = false;

    /**
     * ProcessController constructor.
     * @param bool $config
     */
    public function __construct($config = false)
    {
        if ($config) {
            $this->config = $config;
        }
    }

    /**
     * @param Request $request
     * @param Application $app
     * @return mixed
     * @deprecated Deprecated due memory exhaustion
     *
     * @throws \RuntimeException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function process(Request $request, Application $app)
    {
        trigger_error('Deprecated due memory exhaustion');
        $finder = $this->getFinder();

        set_time_limit(-1);
        ini_set('memory_limit', '128M');

        $users = [];
        $users_debug = [];
        $i = 0;

        if ($this->getConfig('app', 'save_method') == 'during') {
            $this->prepareToSingle($app['db']);
        }

        /** @var \Twig_Environment $twig */
        $twig = $app['twig'];

        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $contents = $file->getContents();
            preg_match_all($this->getConfig('app', 'row_expression'), $contents, $matches);

            foreach ($matches[0] as $match) {
                $name = $file->getRelativePathname();
                preg_match($this->getConfig('app', 'date_regexp'), $name, $dateMatches);

                if (self::$debug && $this->getConfig('app', 'save_method') == 'final') {
                    $users_debug[] = $name . ': ' . $match;
                }

                $user_encoded = json_decode('{' . $match . '}');

                if ($user_encoded->user_id != 1) {
                    if ($this->getConfig('app', 'save_method') == 'during') {
                        $this->saveSingle($app['db'], $dateMatches[0], $user_encoded->user_id, $user_encoded->ip);
                        $users[] = $i;
                        $i++;
                    } else {
                        $users[] = [
                            'date' => $dateMatches[0],
                            'user_id' => $user_encoded->user_id,
                            'ip' => $user_encoded->ip
                        ];
                    }
                }
            }

            unset($contents, $matches);
        }

        if (self::$debug && $this->getConfig('app', 'save_method') == 'final') {
            $this->saveDebug($users_debug);
        }

        if (!empty($users) && $this->getConfig('app', 'save_method') == 'final') {
            $this->saveResults($app['db'], $users);
        }

        return $twig->render(
            'result.html.twig',
            [
                'app' => $app,
                'usersCount' => count($users)
            ]
        );
    }

    /**
     * @return Finder
     */
    private function getFinder()
    {
        $finder = new Finder();
        $finder->files();

        //add extension
        $extensions = $this->getConfig('app', 'allowed_extension');

        if (!empty($extensions)) {
            foreach ($extensions as $extension) {
                $finder->name($extension);
            }
        }

        //add sources
        $sources = $this->getConfig('app', 'working_dir');
        if (!empty($sources)) {
            foreach ($sources as $source) {
                $finder->in(BASE_DIR . DS . $source);
            }
        }

        //add name
        $allowedFiles = $this->getConfig('app', 'allowed_file');
        if (!empty($allowedFiles)) {
            foreach ($allowedFiles as $allowedFile) {
                $finder->name($allowedFile);
            }
        }

        //add exceptions
        $disallowedFiles = $this->getConfig('app', 'disallowed_file');
        if (!empty($disallowedFiles)) {
            foreach ($disallowedFiles as $disallowedFile) {
                $finder->notName($disallowedFile);
            }
        }

        return $finder;
    }

    /**
     * @param bool $key
     * @param bool $scope
     * @return bool|mixed
     */
    private function getConfig($scope = false, $key = false)
    {
        if ($scope && $key) {
            return $this->config[$scope][$key];
        }

        return $this->config;
    }

    /** @param Connection $db */
    private function prepareToSingle($db)
    {
        $db->exec(
            'CREATE TABLE IF NOT EXISTS `log` (`id` bigint(20) NOT NULL AUTO_INCREMENT, `date` date NOT NULL,`user_id` bigint(20) UNSIGNED NOT NULL, `ip` varchar(45) DEFAULT NULL, `hits` int(10) UNSIGNED DEFAULT 0, PRIMARY KEY (`id`), INDEX IDX_log_date (date) ) ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET utf8 COLLATE utf8_general_ci'
        );

        if ($this->getConfig('app', 'truncate_db_table') == 1) {
            $db->exec('TRUNCATE TABLE `log`');
        }
    }

    /** @param Connection $db */
    private function saveSingle($db, $date, $user_id, $ip)
    {
        $hitCounter = $db->fetchColumn(
            'SELECT COUNT(*) FROM `log` WHERE `log`.date = ? AND `log`.ip = ? AND `log`.user_id <> ?',
            [$date, $ip, $user_id]
        );

        $db->insert('log', array(
            'date' => $date,
            'user_id' => $user_id,
            'ip' => $ip,
            'hits' => $hitCounter
        ));

    }

    /**
     * @param array $debugArray
     */
    private function saveDebug(array &$debugArray)
    {
        if (is_file('debug.csv')) {
            unlink('debug.csv');
        }
        $handle = fopen('debug.csv', 'w+');

        foreach ($debugArray as $row) {
            fputcsv(
                $handle,
                [$row],
                ';'
            );
        }

        fclose($handle);
    }

    /** @param Connection $db */
    private function saveResults($db, $results)
    {
        $db->exec(
            'CREATE TABLE IF NOT EXISTS `log` (`id` bigint(20) NOT NULL AUTO_INCREMENT, `date` date NOT NULL,`user_id` bigint(20) UNSIGNED NOT NULL, `ip` varchar(45) DEFAULT NULL, `hits` int(10) UNSIGNED DEFAULT 0, PRIMARY KEY (`id`), INDEX IDX_log_date (date) ) ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET utf8 COLLATE utf8_general_ci'
        );

        if ($this->getConfig('app', 'truncate_db_table') == 1) {
            $db->exec('TRUNCATE TABLE `log`');
        }

        foreach ($results as $result) {
            $hitCounter = $db->fetchColumn(
                'SELECT COUNT(*) FROM `log` WHERE `log`.date = ? AND `log`.ip = ? AND `log`.user_id <> ?',
                [$result['date'], $result['ip'], $result['user_id']]
            );

            $db->insert('log', array(
                'date' => $result['date'],
                'user_id' => $result['user_id'],
                'ip' => $result['ip'],
                'hits' => $hitCounter
            ));
        }
    }

    /**
     * @param Request $request
     * @param Application $app
     * @return JsonResponse
     */
    public function single(Request $request, Application $app)
    {
        if (!$request->get('filename')) {
            return new JsonResponse(json_encode('no filename passed'), 403);
        }
        $finder = $this->getFinder();
        set_time_limit(-1);
        ini_set('memory_limit', '128M');

        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $name = $file->getRelativePathname();
            if ($name == $request->get('filename')) {
                if ($this->getConfig('app', 'save_method') == 'during') {
                    $this->prepareToSingle($app['db']);
                }

                $this->processFile($app, $file);
            }
        }

        return new JsonResponse(json_encode('ok'), 200);
    }

    /**
     * @param Application $app
     * @param $file
     */
    private function processFile(Application $app, SplFileInfo $file)
    {
        $contents = $file->getContents();
        $matches = 0;

        preg_match_all($this->getConfig('app', 'row_expression'), $contents, $matches);

        foreach ($matches[0] as $match) {
            $name = $file->getRelativePathname();
            preg_match($this->getConfig('app', 'date_regexp'), $name, $dateMatches);

            $user_encoded = json_decode('{' . $match . '}');

            if ($user_encoded->user_id != 1) {
                $this->saveSingle($app['db'], $dateMatches[0], $user_encoded->user_id, $user_encoded->ip);
                $matches++;
            }
        }

        unset($contents, $matches);
    }
}
