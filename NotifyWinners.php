<?php
namespace Ae\Bolixrace\Classes;

/**
 * Plik należy do wewnętrzenego projektu dla firmy Bolix - konkursu dla dystrybutorów na najlepszą sprzedaż roczną.
 * Poniższa klasa odpowiada za informowanie uczestników co rundę o postępach.
 * Data utworzenia: 1 kwartał 2017
 */

use Ae\Bolixrace\Models\Positions;
use Ae\Bolixrace\Models\Rounds;
use Backend\Models\User as BackendUser;
use Illuminate\Database\Eloquent\Collection;
use RainLab\User\Models\User;

class NotifyWinners
{
    protected $date = null;

    /**
     * @var null|Rounds
     */
    protected $round = null;

    public function process()
    {
        if (empty($this->date)) {
            $this->date = new \DateTime('now');
        }

        $rounds = Rounds::all();

        foreach ($rounds as $round) {
            if ($round->getEndDate()->format('Y-m-d') == $this->date->format('Y-m-d')) {
                $this->round = $round;

                $winners = $this->getWinners();
                $admins = $this->getAdmins();

                if ($winners->isEmpty()) {
                    \Log::info(__NAMESPACE__ . '/' . __CLASS__ . ':' . 'Round ended without winners.');
                } else {
                    $this->notify($winners, $admins);
                }
            }
        }
    }

    /**
     * @return mixed|Collection|static[]
     */
    protected function getWinners()
    {
        $racers = Positions::getList(Positions::getMaxDate($this->round->id), $this->round->id);
        $winners = new Collection();

        for ($i = 1; $i <=3; $i++) {
            if (!$racers->isEmpty()) {
                $winners->prepend($racers->shift());
            }
        }

        return $winners;
    }

    /**
     * @return Collection|static[]
     */
    protected function getAdmins()
    {
        return BackendUser::all();
    }

    protected function notify(Collection $winners, Collection $admins)
    {
        $users = new Collection();

        foreach ($winners as $contestant) {
            $user = User::query()->where('dealer_id', $contestant->dealer_id)->first();
            $users->push($user);

            if (!empty($user->email)) {
                $this->sendUser($user->email, $contestant->position);
            }
        }

        foreach ($admins as $admin) {
            $this->sendAdmin($admin->email, $users);
        }
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @param $targetEmail
     * @param $position
     */
    protected function sendUser($targetEmail, $position)
    {
        \Mail::sendTo($targetEmail, 'ae.bolixrace::mail.winner_position', [
            'position' => $position,
            'round' => $this->round,
        ]);
    }

    /**
     * @param $targetEmail
     * @param $users
     */
    protected function sendAdmin($targetEmail, $users)
    {
        \Mail::sendTo($targetEmail, 'ae.bolixrace::mail.winner_mail_for_admins', [
            'users' => $users,
            'round' => $this->round,
        ]);
    }
}
